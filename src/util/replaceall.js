export default (data,search,replace) => {
  return data.split(search).join(replace);
}
