export default function(data,fileName){
  let blob = new Blob([data.toString()], {
      type: "text/csv;charset=utf-8"
  });
  let url = URL.createObjectURL(blob);
  let a = document.createElement('a');
  a.download = fileName;
  a.href = url;
  a.textContent = "Download "+fileName;
  a.click();
}
