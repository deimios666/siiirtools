export default (pageSize,maxRecords)=>{
  let iterable = [];
  let maxPages = Math.ceil(maxRecords / pageSize);
  for (let pageCounter = 0; pageCounter < maxPages; pageCounter++) {
      iterable[pageCounter] = {
          page: pageCounter + 1,
          start: pageCounter * pageSize
      };
  }
  return iterable;
}
