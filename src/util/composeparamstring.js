function urlMapperFunction(item, value, index) {
    //If it's odd we resolve from item and urlencode else just return it
    if (index % 2 == 1) {
        return encodeURIComponent('' + value.split('.').reduce((o, i) => o[i], item));
    }
    return value;
}

export default (paramArray,item)=>{
  return paramArray.map(urlMapperFunction.bind(this, item)).join('');
}
