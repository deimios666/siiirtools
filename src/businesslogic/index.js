import async from 'async';

import saveAs from '../util/saveas';
import replaceAll from '../util/replaceall';
import paginate from '../util/paginate';
import composeparamstring from '../util/composeparamstring';

const exporter = ({
    listUrl,
    listParams,
    headers,
    values,
    fileName,
    pageSize,
    maxParallel
} = {}) => {
    let csvData = [];

    //get nr records
    fetch(listUrl + "?_dc=" + (new Date).getTime(), {
            method: 'post',
            credentials: 'same-origin',
            headers: {
                "Accept": "application/json",
                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
            },
            body: listParams + "&page=1&start=0&limit=1"
        })
        .then(data => data.json())
        .then(jsonData => {
            //create pagination
            let totalRecords = jsonData.page.total;
            let pages = paginate(pageSize, totalRecords);
            //download data
            async.eachLimit(
                pages,
                maxParallel,
                (item, callback) => {
                    fetch(listUrl + "?_dc=" + (new Date).getTime(), {
                            method: 'post',
                            credentials: 'same-origin',
                            headers: {
                                "Accept": "application/json",
                                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                            },
                            body: listParams + "&page=" + item.page + "&start=" + item.start + "&limit=" + pageSize
                        })
                        .then((response) => response.json())
                        .then(((item, callback, responseJSON) => {
                            responseJSON.page.content.forEach(
                                (item) => {
                                    if (item.id == null) {
                                        return;
                                    }
                                    var oneLine = [];
                                    //parse and generate line, escape double quotes by doubling them according to CSV standard
                                    for (let i = 0; i < headers.length; i++) {
                                        try {
                                            var value = values[i].split('.').reduce((o, i) => o[i], item);
                                            if (typeof value === "string") {
                                                value = replaceAll(value, '"', '""')
                                            }
                                            if (value == null) {
                                                oneLine.push('""');
                                            } else {
                                                oneLine.push('"' + value + '"');
                                            }
                                        } catch (err) {
                                            oneLine.push('""');
                                        }
                                    }
                                    csvData.push(oneLine.join(','));
                                }
                            );
                            callback();
                        }).bind(this, item, callback))
                },
                (err) => {
                    if (err) {
                        alert(err);
                        return;
                    }
                    //create CSV
                    let buffer = "\uFEFF" + headers.join(",") + "\n" + csvData.join("\n");
                    //download CSV
                    saveAs(buffer, fileName);
                }
            );

        })
}

const deepExporter = ({
    listUrl,
    listParams,
    actionUrl,
    actionParams,
    parentHeaders,
    parentValues,
    headers,
    values,
    fileName,
    pageSize,
    maxParallel,
    setProgress,
    setBusy
} = {}) => {
    setBusy(true);
    let csvData = [];
    let progressCounter = 0;
    let progress = 0;
    let timer = setInterval(()=>{setProgress(progress)},1000);


    //get nr records
    fetch(listUrl + "?_dc=" + (new Date).getTime(), {
            method: 'post',
            credentials: 'same-origin',
            headers: {
                "Accept": "application/json",
                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
            },
            body: listParams + "&page=1&start=0&limit=1"
        })
        .then(data => data.json())
        .then(jsonData => {
            //create pagination
            let totalRecords = jsonData.page.total;
            let pages = paginate(pageSize, totalRecords);
            //download data
            async.eachLimit(
                pages,
                maxParallel,
                (page, callback) => {
                    fetch(listUrl + "?_dc=" + (new Date).getTime(), {
                            method: 'post',
                            credentials: 'same-origin',
                            headers: {
                                "Accept": "application/json",
                                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                            },
                            body: listParams + "&page=" + page.page + "&start=" + page.start + "&limit=" + pageSize
                        })
                        .then((response) => response.json())
                        .then(((item, callback, responseJSON) => {
                            //for each line in a page
                            async.eachLimit(
                                responseJSON.page.content,
                                maxParallel,
                                (line, subcallback) => {
                                    //fetch data for line
                                    fetch(actionUrl + "?_dc=" + (new Date).getTime(), {
                                            method: 'post',
                                            credentials: 'same-origin',
                                            headers: {
                                                "Accept": "application/json",
                                                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                                            },
                                            body: composeparamstring(actionParams, line)
                                        })
                                        .then((response) => response.json())
                                        .then(((line, subcallback, subResponseJSON) => {
                                            subResponseJSON.page.content.forEach(
                                                (jsonLine) => {
                                                    if (jsonLine.id == null) {
                                                        return;
                                                    }

                                                    var oneLine = [];

                                                    //check if parentHeaders exists
                                                    //resolve from parent
                                                    if (parentHeaders){
                                                      for (let i = 0; i < parentHeaders.length; i++) {
                                                        try {
                                                              var value = parentValues[i].split('.').reduce((o, i) => o[i], line);

                                                              if (typeof value === "string") {
                                                                  value = replaceAll(value, '"', '""')
                                                              }
                                                              if (value == null) {
                                                                  oneLine.push('""');
                                                              } else {
                                                                  oneLine.push('"' + value + '"');
                                                              }
                                                          } catch (err) {
                                                              oneLine.push('""');
                                                          }
                                                        }
                                                    }
                                                    
                                                    //parse and generate line, escape double quotes by doubling them according to CSV standard
                                                    for (let i = 0; i < headers.length; i++) {
                                                        try {
                                                            var value = values[i].split('.').reduce((o, i) => o[i], jsonLine);

                                                            if (typeof value === "string") {
                                                                value = replaceAll(value, '"', '""')
                                                            }
                                                            if (value == null) {
                                                                oneLine.push('""');
                                                            } else {
                                                                oneLine.push('"' + value + '"');
                                                            }
                                                        } catch (err) {
                                                            oneLine.push('""');
                                                        }
                                                    }
                                                    csvData.push(oneLine.join(','));
                                                });
                                            progressCounter++;
                                            progress = Math.round(progressCounter / totalRecords * 100);
                                            subcallback();
                                        }).bind(this, line, subcallback))
                                },
                                err => {
                                    callback();
                                })
                        }).bind(this, page, callback))
                },
                (err) => {
                    setBusy(false);
                    clearInterval(timer);
                    progress=0;
                    setProgress(progress);
                    if (err) {
                        alert(err);
                        return;
                    }
                    //create CSV
                    let buffer = "\uFEFF";
                    if (parentHeaders){
                      buffer = buffer + parentHeaders.join(",") + ",";
                    }
                    buffer = buffer + headers.join(",") + "\n" + csvData.join("\n");
                    //download CSV
                    saveAs(buffer, fileName);
                }
            );

        })
        .catch(err =>{
          alert("Trebuie să fiți autentificați în SIIIR");
          setBusy(false);
        });
}

export default {
    exporter,
    deepExporter
}