import config from '../config';

const yearUrl = "list/schoolYear.json";

const getSchoolYears = () => {

    let url = config.url.base + yearUrl;

    return fetch(url + "?_dc=" + (new Date).getTime(), {
            method: 'post',
            credentials: 'same-origin',
            headers: {
                "Accept": "application/json",
                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
            },
            body: "page=1&start=0&limit=100"
        })
}

export default getSchoolYears
