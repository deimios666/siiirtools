import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';

import App from './App';
import store from './store';


const body = document.getElementsByTagName("body")[0];
const app = document.createElement('div');
app.id = 'root';
if (body) { body.prepend(app) };

ReactDOM.render(<Provider store={store}>
  <App />
</Provider>, document.getElementById('root'));
