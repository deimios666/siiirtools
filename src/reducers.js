import {
    combineReducers
} from 'redux';

const drawerReducer = (state, action) => {

    if (!state) {
        return {
            open: false
        }
    }

    switch (action.type) {
        case "OPEN_DRAWER":
            {
                return {...state,
                    open: true
                };
            }
        case "CLOSE_DRAWER":
            {
                return {...state,
                    open: false
                };
            }
    }

    return state;
}

const paramReducer = (state, action) => {

    if (!state) {
        return {
            schoolYearId: 20
        }
    }

    switch (action.type) {
        case "CHANGE_SCHOOLYEARID":
            {
                return {
                  ...state,
                  schoolYearId: action.payload
                };
            }
    }

    return state;
}

const schoolYearReducer = (state, action) => {

    if (!state) {
        return {
            isLoaded: false,
            schoolYears: [{
                id: 20,
                code: "2017-2018"
            }]
        }
    }

    switch (action.type) {
        case "LOAD_SCHOOLYEARS":
            {
                return {
                    ...state,
                    isLoaded: true,
                    schoolYears: action.payload
                }
            }
    }

    return state;
}

const progressReducer = (state, action) => {

    if (!state) {
        return {
            progress: 0,
            busy: false
        }
    }

    switch (action.type) {
        case "CHANGE_PROGRESS":
            {
                let progress = action.payload;
                if (progress < 0) {
                    progress = 0;
                } else if (progress > 100) {
                    progress = 100
                }
                return {...state,
                    progress: progress
                };
            }
        case "CHANGE_BUSY":
            {
                return {...state,
                    busy: action.payload
                };
            }
    }
    return state;
}

const reducers = combineReducers({
    drawer: drawerReducer,
    params: paramReducer,
    progress: progressReducer,
    schoolYears: schoolYearReducer
});

export default reducers;