import React, { Component } from 'react';

import {connect} from "react-redux";

import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import SelectField from 'material-ui/SelectField';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import LinearProgress from 'material-ui/LinearProgress';


import bl from './businesslogic';
import getSchoolYears from './businesslogic/schoolyeargetter';
import exportersFactory from './businesslogic/exporters';

import {
  openDrawer,
  closeDrawer,
  changeSchoolYearId,
  changeProgress,
  changeBusy,
  loadSchoolYears
} from './actions';

import './App.css';

class App extends Component {

  handleOpen = () => {
    if (!this.props.schoolYears.isLoaded){
      getSchoolYears()
      .then(data => data.json())
      .then(jsonData => {
        this.props.dispatch(loadSchoolYears(jsonData.page.content.map(
            item=>({
              id:item.id,
              code:item.code
            })
          )));
      })
      .catch((err)=>console.error(err));

    }
    return this.props.dispatch(openDrawer())
  };
  handleClose = () => this.props.dispatch(closeDrawer());
  handleSchoolYearChange = (event, index, value) => this.props.dispatch(changeSchoolYearId(value));
  setProgress = (value) => this.props.dispatch(changeProgress(value));
  setBusy = (value) => this.props.dispatch(changeBusy(value));

  render() {
    let exporters = exportersFactory(this.props.params.schoolYearId);

    let progressTracker = {progress:0};

    let rows = [];
    exporters.forEach(
      (exporter,index) => {
        if (exporter.actionUrl){
          rows.push(<MenuItem key={index} disabled={this.props.progress.busy} onClick={()=>{bl.deepExporter({...exporter, schoolYearId:this.props.params.schoolYearId, setProgress:this.setProgress, setBusy:this.setBusy})}}>{exporter.shortName}</MenuItem>);
        } else {
          rows.push(<MenuItem key={index} disabled={this.props.progress.busy} onClick={()=>{bl.exporter({...exporter, schoolYearId:this.props.params.schoolYearId, setProgress:this.setProgress, setBusy:this.setBusy})}}>{exporter.shortName}</MenuItem>);
        }
      }
    );

    let schoolYearArray = [];

    this.props.schoolYears.schoolYears.forEach(
        (item, index) => {
            schoolYearArray.push(<MenuItem key={item.id} value={item.id} primaryText={item.code} />);
        }
    );


    return (
      <MuiThemeProvider>
        <div className="App">
          <RaisedButton label="ST" onClick={this.handleOpen} />
          <Drawer
            docked={false}
            open={this.props.drawer.open}
            onRequestChange={(open) => this.handleClose()}
            >

            <SelectField
              floatingLabelText="An Școlar"
              value={this.props.params.schoolYearId}
              onChange={this.handleSchoolYearChange}>
              {schoolYearArray}
            </SelectField>
            <LinearProgress mode="determinate" value={this.props.progress.progress} />
            {rows}
          </Drawer>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default connect(state => state)(App);
