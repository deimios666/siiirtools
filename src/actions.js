export function openDrawer() {
  return {
    type: "OPEN_DRAWER",
    payload: null
  }
}

export function changeDrawer(open) {
  if (open){
    return {
      type: "OPEN_DRAWER",
      payload: null
    }
  } else {
    return {
      type: "CLOSE_DRAWER",
      payload: null
    }
  }
}

export function closeDrawer() {
  return {
    type: "CLOSE_DRAWER",
    payload: null
  }
}

export function changeSchoolYearId(schoolYearId){
  return {
    type: "CHANGE_SCHOOLYEARID",
    payload: schoolYearId
  }
}

export function loadSchoolYears(schoolYears){
  return {
    type: "LOAD_SCHOOLYEARS",
    payload: schoolYears
  }
}

export function changeProgress(progress){
  return {
    type: "CHANGE_PROGRESS",
    payload: progress
  }
}

export function changeBusy(busy){
  return {
    type: "CHANGE_BUSY",
    payload: busy
  }
}

export function setBusy(){
  return {
    type: "CHANGE_BUSY",
    payload: true
  }
}

export function unsetBusy(){
  return {
    type: "CHANGE_BUSY",
    payload: false
  }
}
